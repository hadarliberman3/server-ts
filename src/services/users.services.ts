import { Iuser,IuserServiceResponse } from "../types.js";
import { uid } from "../utils/utils-funcs.js";
import { MyErrorException, URLnotFound } from "../exceptions/errors.js";

export class UserServices{

 serviceAddUser(reqName:string, reqAge:string, users:Iuser[]):IuserServiceResponse{
    if(reqName===undefined||reqAge===undefined){
     throw new MyErrorException(400,'invalid user detailes');
    }
    const newUser:Iuser={
        id:uid(),
        name:reqName,
        age:Number(reqAge)
    }
    users.push(newUser);
    const response:IuserServiceResponse={
        usersDB:[...users],
        data:newUser
    }
    return response;
}

serviceEditUser(reqId:string, reqName:string, reqAge:string,reqPath:string,users:Iuser[]):IuserServiceResponse{
    const updateIndex=users.findIndex(user=>user.id==reqId);
    if(updateIndex===-1){
        throw new URLnotFound(reqPath);
    }
    const newUser:Iuser={
        id:String(reqId),
        name:String(reqName),
        age:Number(reqAge)
    }
    users[updateIndex]=newUser;
    const response:IuserServiceResponse={
        usersDB:[...users],
        data:newUser
    }
    return response;
    
}

 serviceDeleteUser(reqId:string, reqPath:string, users:Iuser[]):IuserServiceResponse{
    const newUsers=users.filter((user)=>user.id!=reqId); 
    if(newUsers.length===users.length){
        throw new URLnotFound(reqPath);
    } 
    const response:IuserServiceResponse={
        usersDB:[...users]  
    }
    return response;
}


serviceShowOne(reqId:string,reqPath:string,users:Iuser[]){
    const user=users.filter(cur=>cur.id==reqId);
    if(user.length<1){ 
        throw new URLnotFound(reqPath);
    }else return user;
}

}

