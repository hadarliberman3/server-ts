import { Request, Response, NextFunction} from 'express';
import { Iresponse } from "../types.js";
import UserModel from "../db/users.model.js";
import {UserServices} from "../services/users.services.js";

const user_services=new UserServices();

export async function showAll(req:Request,res:Response,next:NextFunction){
    try{
    res.status(200).json(req.users);
    }
    catch(err){
        next(err);
    }

}

export async function showOne(req:Request,res:Response,next:NextFunction){
    try{
    const user=user_services.serviceShowOne(req.params.id,req.path,req.users);
        res.status(200).json(user);
    
}catch(error){
    next(error);
}

}

export async function deleteUser(req:Request,res:Response,next:NextFunction){
    try{
    const serviceRes=user_services.serviceDeleteUser(req.params.id,req.path,req.users);
    const output:Iresponse={
        status:200,
        message:`user was deleted`,
        data:`id: ${req.params.id}`
    }  
    UserModel.writeDB(serviceRes.usersDB);
    res.status(output.status).json(output);
}catch(err){
    next(err);
  }

}

export async function editUser(req:Request,res:Response,next:NextFunction){
    try{
     const serviceRes=user_services.serviceEditUser(req.params.id,req.body.name,req.body.age,req.path,req.users);  
     req.users=serviceRes.usersDB;
     const output:Iresponse={
        status:200,
        message:`user was edit`,
        data:serviceRes.data
    }
    
    UserModel.writeDB(req.users);
    res.status(output.status).json(output);
}
catch(err){
    next(err);
  }

}

export async function addUser(req:Request,res:Response,next:NextFunction){
    try{
    const serviceRes=user_services.serviceAddUser(req.body.name,req.body.age, req.users);    
    req.users=serviceRes.usersDB; 
    console.log(serviceRes);
    const output:Iresponse={
            status:200,
            message:`new user added`,
            data:serviceRes.data
        }
    UserModel.writeDB(req.users);
    res.status(output.status).json(output);
    }
    catch(err){
        next(err);
        }

}
