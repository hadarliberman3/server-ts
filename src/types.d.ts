export interface Iuser{
    id:string,
    name:string,
    age:number
}

export interface IresponseError{
  status:number,
  message:string,
  stack?:string
}
export interface Iresponse{
  status:number,
  message:string,
  data:any
}

export interface IuserServiceResponse{
  usersDB:Iuser[],
  data?:Iuser
}

declare global {

    namespace Express {
      interface Request {
          reqId:string,
          users: IUser[];
      }
    }
  }