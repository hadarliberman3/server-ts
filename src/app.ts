import log from "@ajar/marker";
import { Request, Response, NextFunction, RequestHandler} from 'express';
import express from "express";
import usersRouter from './routers/usersRouter.js';
import UserModel from "./db/users.model.js";
import path, { dirname } from "path";
import { fileURLToPath } from "url";
import {PrintErrorMW,logToFileErrorMW,ResponseErrorMW,URLnotFoundMW} from "./middlwares/errors.middlware.js";
import { httpLogs } from "./middlwares/http.log.middlware.js";
import { reqId } from "./middlwares/genrateId.middleware.js";
const __dirname = dirname(fileURLToPath(import.meta.url));

const{PORT,HOST='localhost'}=process.env;
const app=express();

const DBpath:string=path.resolve(__dirname,"../src/db/database.json");
const LOG_USER_ERR_path:string=path.resolve(__dirname,"../src/logs/users.errors.log");
const LOG_HTTP_path:string=path.resolve(__dirname,"../src/logs/users.http.log");

app.use(express.json());
app.use(reqId);
app.use(httpLogs(LOG_HTTP_path));
app.use('/api/users',usersRouter);

app.use(URLnotFoundMW);
app.use(PrintErrorMW);
app.use(logToFileErrorMW(LOG_USER_ERR_path));
app.use(ResponseErrorMW);

app.listen( Number(PORT), HOST,  async ()=> {
    await UserModel.initialDB(DBpath);
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});