
import { Request, Response, NextFunction, RequestHandler} from 'express';
import { uid } from '../utils/utils-funcs.js';


export const reqId:RequestHandler=async function(req:Request,res:Response,next:NextFunction){
    req.reqId=uid();
    next();
}