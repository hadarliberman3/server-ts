import { Console } from 'console';
import { RSA_NO_PADDING } from 'constants';
import fs from 'fs/promises';
import { Iuser } from '../types.js';
import { MyErrorException } from '../exceptions/errors.js';

class UserModel{

    DBPath:string='';
    DB:Iuser[]=[];

    async initialDB(filePath:string){
    this.DBPath=filePath;
    // console.log(this.DBPath);
    try{
        this.DB=JSON.parse(await fs.readFile(this.DBPath,"utf-8"));
        // console.log(this.DB);
        }
    catch(err){
        const arr:Iuser[]=[];
        console.log("initial");
        await fs.writeFile(filePath,JSON.stringify(arr));
        this.DB=[];
        }
    
    }

    async readDB(){
        try{
        this.DB=JSON.parse(await fs.readFile(this.DBPath,"utf-8"));
        }catch(err){
            throw new MyErrorException(500,"cant read from db");
        }
    }

    async writeDB(users:Iuser[]){ 
        try{
        await fs.writeFile(this.DBPath,JSON.stringify(users,null,2)); 
        }catch(err){
            throw new MyErrorException(500,"cant read from db");

            
        }
    }
}

const usersInstance=new UserModel();
export default usersInstance; 
