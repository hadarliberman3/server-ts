import log, { err } from "@ajar/marker";
import { Request, Response, NextFunction, RequestHandler} from 'express';
import express from "express";
// import usersRouter from './users.js';
import fs from "fs/promises";
import { MyErrorException, URLnotFound } from "../exceptions/errors.js";
import { IresponseError } from "../types.js";

export function PrintErrorMW(error:MyErrorException, req: Request, res: Response, next: NextFunction){
    console.log(`error status: ${error.status} error message: ${error.message}`);
    next(error);
}

export function logToFileErrorMW(filePath:string){
    console.log(filePath);
    return async (error:MyErrorException, req: Request, res: Response, next: NextFunction)=>{
        let errors=await fs.readFile(filePath,"utf-8");
        console.log(errors);
        errors+=`${error.status} || ${error.stack}}\n`;
        await fs.writeFile(filePath,errors);
        next(error);
}
}

export function ResponseErrorMW(error:MyErrorException, req: Request, res: Response, next: NextFunction){
    const output:IresponseError={
        status:error.status||500,
        message:error.message||'http error',
    }
    res.status(output.status).json(output);
}

export function URLnotFoundMW(req: Request, res: Response, next: NextFunction){
    next(new URLnotFound(req.url));
}



