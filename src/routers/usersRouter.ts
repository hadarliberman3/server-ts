import { Router } from "express";
import { Request, Response, NextFunction } from 'express';
import UserModel from "../db/users.model.js";
import { deleteUser, showAll, showOne,editUser, addUser } from "../controller/users.controller.js";

const router=Router();

router.use(async (req:Request,res:Response,next:NextFunction)=>{
    try {
        await UserModel.readDB();  
        req.users = UserModel.DB;
        next();
      } catch (err) {
        next(err);
      }
})


//add
router.post('/',addUser);

//edit
router.put('/:id',editUser);

//delete
router.delete('/:id',deleteUser);

//get one
router.get('/:id',showOne);

//get all
router.get('/',showAll);


export default router;