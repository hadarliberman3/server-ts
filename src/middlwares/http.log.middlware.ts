
import { Request, Response, NextFunction, RequestHandler} from 'express';
import fs from "fs/promises";


export function httpLogs(filePath:string){
    return async function(req:Request,res:Response,next:NextFunction){
    let logs=await fs.readFile(filePath,"utf-8");
    // console.log('logs:'+logs);
    logs+=`${req.method} ${req.path} ${req.reqId} ${Date.now()}\n`;
    await fs.writeFile(filePath,logs);
    next();
    }
}